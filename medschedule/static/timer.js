var timer = new easytimer.Timer();
var min=10;
var refr = 200;
var timeoutVal = 500;

const urlParams = new URLSearchParams(window.location.search);
const minReq = parseInt(urlParams.get('minutes'));
if(minReq > 0)
    min = parseInt(minReq);

function updateTime(timeVal){
    var timeString = "";
    if (timeVal < 10)
        timeString += "0" 
    timeString += timeVal.toString()+":00"
    $('#time').html(timeString);
};

updateTime(min);

$('#addMinute').click(function () {
    if(min<60){min += 1};
    updateTime(min);
});

$('#subMinute').click(function () {
    if(min>1){min -= 1};
    updateTime(min);
});

$('#startButton').click(function () {
    timer.start({countdown: true, startValues: {minutes: min, seconds: 0}});
    $('#time').html(timer.getTimeValues().toString().substring(3));
});

$('#stopButton').click(function () {
    timer.reset()
    timer.stop()
    if(minReq > 0)
    min = parseInt(minReq);
    updateTime(min)
});

timer.addEventListener('secondsUpdated', function (e) {
    $('#time').html(timer.getTimeValues().toString().substring(3));
});

