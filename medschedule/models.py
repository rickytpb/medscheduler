from django.db import models
from django.contrib.auth.models import User

days = ['mon','tue','wed','thu','fri','sat','sun']

class MeditationSchedule(models.Model):
    name = models.CharField(max_length=255, default="default")
    days = models.CharField(max_length=21)
    duration = models.SmallIntegerField()
    time = models.CharField(max_length=5, default="10:00")
    owner = models.ForeignKey(User, related_name='schedules', on_delete=models.CASCADE)

    def dateToString(self):
        return ' | '.join([self.days[i:i+3] for i in range(0, len(self.days), 3)])


#Implement content links db that randomizes content
#Possibly with some options
class ContentAggregator(models.Model):
    pass
    
