from django.apps import AppConfig


class MedscheduleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'medschedule'
