import base64
import json
import html
import requests
import random
from urllib.error import HTTPError

class AbstractApiService():
    def __init__(self, url):
        self._url = url

    def _compose_uri(self, path):
        return f"{self._url}/{path}"

    def _get_json_simple(self, uri, auth=None, headers=None, params=None):
        r = requests.get(uri, auth=auth, headers=headers, params=params)
        if str(r.status_code)[0] != 2:
            r.raise_for_status()
        return json.loads(r.text)

    @classmethod
    def _post_token(self, uri, headers, params=None):
        r = requests.post(uri, headers=headers, params=params)
        if r.status_code != 200:
            r.raise_for_status()
        return json.loads(r.text)
        

class QuoteApiService(AbstractApiService):
    def get_quote_of_the_day(self, language, category):
        path = "qod"
        p = {'language':language, 'category':category}
        try:
            payload = self._get_json_simple(self._compose_uri(path), params=p)
            print(payload)
            quote = payload['contents']['quotes'][0]
        except requests.HTTPError as e:
            return {"quote": "When I let go of what I am, I become what I might be.","author": "Lao Tzu"}
        return {"quote":quote['quote'], "author":quote['author']}


class SpotifyApiService(AbstractApiService):
    client_id = 'b5067dfb826943ffb010872b3676a145'
    client_secret = '543524bc1ec94e41b52b1acd37b9c682'

    @classmethod
    def __get_creds_base64(cls):
        return base64.b64encode(str(f"{cls.client_id}:{cls.client_secret}").encode('ascii')).decode('utf-8')

    @classmethod
    def get_new_token(cls):
        params ={'grant_type':'client_credentials','json':'true'}
        headers = {'Authorization' : f'Basic {cls.__get_creds_base64()}', 'Content-Type':'application/x-www-form-urlencoded'}
        resp = cls._post_token('https://accounts.spotify.com/api/token',headers,params)
        cls.token = resp['access_token']

    @classmethod
    def is_token_set(cls):
        try:
            if cls.token:
                return True 
        except AttributeError:
            return False

    def get_recommended_guided_tracks(self, type='playlist', query="guided meditation", n=3):
        path = 'v1/search'
        params = {'q': html.escape(query),'type':type,'limit':n}
        headers = {'Authorization' : f"Bearer {SpotifyApiService.token}",'Accept':'application/json','Content-Type':'application/json'}
        payload = self._get_json_simple(self._compose_uri(path),headers=headers, params=params)
        content = []
        for item in payload[f"{type}s"]['items']:
            content.append({'type': type, 'id': item['id']})
        return content 

class YoutubeApiService(AbstractApiService):
    api_key = 'AIzaSyCzHEVAI76vQ470YtHX70xQpcb2NNZG2bw'

    def get_random_meditation_videos(self, query='meditation', n=10):
        path = 'youtube/v3/search'
        params = {'part':'snippet','q':query, 'type':'video', 'max_results':2*n,'key':self.api_key}
        headers = {'Accept': 'application/json'}
        payload = self._get_json_simple(self._compose_uri(path),headers=headers, params=params)
        videos = []
        for item in random.sample(payload['items'],n):
            videos.append({'title':html.unescape(item['snippet']['title']), 'id':item['id']['videoId']})
        return videos


class KoanApiServer():
    def __init__(self, file):
        self.koans = []
        with open(file) as f:
            i = 0
            story = ""
            for line in f.readlines():
                if line[0]=='\t':
                    title = line.strip()
                else:
                    if '%' in line:
                        self.koans.append({'title': title,'text':story.strip(), 'id':i})
                        story = ""
                        i += 1
                    else:
                        story += line

    def get_koan(self,id):
        if id>len(self.koans):
            return False
        return self.koans[id]

    def get_koans(self):
        return self.koans

    def get_titles(self):
        titles = []
        for koan in self.koans:
            title = koan['title'] 
            id = koan['id']
            titles.append({'title':title, 'id':id})
        return titles
    
    def get_koans_by_query(self, query):
        koans = []
        for koan in self.koans:
            if str(query).lower() in koan['text']:
                koans.append(koan)
        return koans



if __name__ == "__main__":
    pass