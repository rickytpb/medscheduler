from django.http import HttpResponse
from django.http.response import Http404, HttpResponseBadRequest, HttpResponseForbidden, HttpResponseNotFound, HttpResponseRedirect
from django.template import loader
from django.template.loader import render_to_string
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.templatetags.static import static
from django.shortcuts import redirect
from django.core.mail import send_mail
from django.http import JsonResponse
from weasyprint import HTML
from .models import days, MeditationSchedule
from .api import QuoteApiService, SpotifyApiService, YoutubeApiService, KoanApiServer
import tempfile
import json
import os



def index(request):
    template = loader.get_template('index.html')
    request.session.set_expiry(3600)
    if not request.session.get('quote'):
        quotes = QuoteApiService('https://quotes.rest')
        quote = quotes.get_quote_of_the_day('en','inspire')
        request.session['quote'] =  True
        request.session['quote_body'] = quote
    context = request.session['quote_body']
    return HttpResponse(template.render(context, request))

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            response = {'status':200, 'message':"user successfully logged in"}
        else:
            response = {'status':403, 'message':"user doesn't exist or password doesn't match"}
        return HttpResponse(json.dumps(response),content_type='application/json')
    else:
        return redirect('index')


def logout_view(request):
    logout(request)
    return redirect('index')

def sign_up_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        user = authenticate(request, username=username, password=password)
        if not User.objects.filter(username=username).exists():
            User.objects.create_user(username,email,password)
            response = {'status':200, 'message':"user successfully created"}
            send_mail('it\s so nice to have you with us','we hope we will help you on your journey. have fun stay mindfull.','minimazen@minimazen.com',[email])
        else:
            response = {'status':400, 'message':"user exists"}
        return HttpResponse(json.dumps(response),content_type='application/json')
    else:
        return redirect('index')

def manage_schedules(request):
    template = loader.get_template('manage.html')
    context = { 'days':days
    }
    return HttpResponse(template.render(context, request))

def create_schedule(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            days = ""
            for key, value in request.POST.items():
                if value == "on":
                    days = days + key
            duration = request.POST['duration']
            time = request.POST['time']
            name = request.POST['name']
            if MeditationSchedule.objects.all().filter(name = name, owner=request.user):
                #shouldn't ever happen due to validation
                return HttpResponseBadRequest("schedule with that name exists")
            medsched = MeditationSchedule(name=name, days=days, duration=duration, time=time, owner=request.user)
            medsched.save()
            return redirect('show_schedules')
        else:
            return redirect('index')
    else:
        return HttpResponseBadRequest("Can't save")

def get_schedule_available(request):
    if request.user.is_authenticated:
        if not MeditationSchedule.objects.all().filter(name = request.GET['name'], owner=request.user):
            return HttpResponse(status=204)
        return HttpResponseForbidden(status=200)
    return HttpResponseBadRequest()
    

def show_schedules(request):
    template = loader.get_template('show.html')
    context = { 'schedule_list': MeditationSchedule.objects.all().filter(owner=request.user)
    }
    return HttpResponse(template.render(context, request))

def delete_schedule(request):
    if request.user.is_authenticated:
        #Django uses only POST requests - that makes me sad
        if request.method == 'POST':
            schedule_name = request.POST['schedule_name']
            MeditationSchedule.objects.all().filter(owner=request.user, name=schedule_name).delete()
            return HttpResponse(status=203)
        else:
            return redirect('index')
    else:
        return HttpResponseBadRequest("Can't delete")


def download_schedules(request):
    schlist = MeditationSchedule.objects.all()
    html_string = render_to_string('show_pdf.html', {'schedule_list': schlist, 'username': request.user.username})
    html = HTML(string=html_string)
    result = html.write_pdf()

    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=schedules.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())
    return response

def use_timer(request):
    template = loader.get_template('timer.html')
    context = { 'days':days
    }
    return HttpResponse(template.render(context, request))

def guided_meditations(request):
    template = loader.get_template('guided.html')
    spot = SpotifyApiService('https://api.spotify.com')
    if not request.session.get('spotify') or not SpotifyApiService.is_token_set():
        SpotifyApiService.get_new_token()
        request.session['spotify'] = True
    n = request.GET.get('n', 3)
    query = request.GET.get('query')
    if query=="" or not query:
        query = "guided meditation"
    spotify_playlists = spot.get_recommended_guided_tracks('playlist',query, n)
    context = { 'spotify_data' : spotify_playlists
    }
    return HttpResponse(template.render(context, request))

def explanation(request):
    template = loader.get_template('explanation.html')
    youtube = YoutubeApiService('https://youtube.googleapis.com')
    n = request.GET.get('n', 3)
    videos = youtube.get_random_meditation_videos('simple guided meditation',n)
    context = { 'videos' : videos
    }
    return HttpResponse(template.render(context,request))

def password_reset(request):
    pass

def third_party_content(request):
    pass 

def get_koan(request):
    k = KoanApiServer('medschedule/static/koan.txt')
    if request.method == 'GET':
        koan = k.get_koan(int(request.GET.get('id',0)))
        if not koan:
             return Http404()
        return JsonResponse(koan,status=200, safe=False)


def get_koans(request):
    k = KoanApiServer('medschedule/static/koan.txt')
    if request.method == 'GET':
        if request.GET.get('query','') != '':
            koans = k.get_koans_by_query(request.GET.get('query'))
        else:
            koans = k.get_koans()
        return JsonResponse(koans,status=200, safe=False)

def get_titles(request):
    k = KoanApiServer('medschedule/static/koan.txt')
    if request.method == 'GET':
        titles = k.get_titles()
        return JsonResponse(titles,status=200, safe=False)