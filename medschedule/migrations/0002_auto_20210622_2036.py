# Generated by Django 3.2.3 on 2021-06-22 20:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medschedule', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContentAggregator',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.RemoveField(
            model_name='meditationschedule',
            name='hour',
        ),
        migrations.RemoveField(
            model_name='meditationschedule',
            name='minute',
        ),
        migrations.AddField(
            model_name='meditationschedule',
            name='name',
            field=models.CharField(default='default', max_length=255),
        ),
        migrations.AddField(
            model_name='meditationschedule',
            name='time',
            field=models.CharField(default='10:00', max_length=5),
        ),
    ]
