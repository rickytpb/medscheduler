from django.urls import path 

from . import views 

urlpatterns = [
    path('',views.index, name='index'),
    path('login/',views.login_view, name='login_view'),
    path('logout/',views.logout_view, name='logout_view'),
    path('manage_schedules/', views.manage_schedules, name='manage_schedules'),
    path('guided/', views.guided_meditations, name='guided_meditations'),
    path('create_schedule/', views.create_schedule, name='create_schedule'),
    path('sign_up_view/', views.sign_up_view, name='sign_up_view'),
    path('show_schedules/',views.show_schedules, name='show_schedules'),
    path('delete_schedule/', views.delete_schedule, name='delete_schedule'),
    path('timer/', views.use_timer, name='timer_view'),
    path('password_reset/',views.password_reset, name='password_reset'),
    path('get_schedule_available/',views.get_schedule_available, name='get_schedule_available'),
    path('download_schedules/',views.download_schedules, name='download_schedules'),
    path('explanation/',views.explanation, name='explanation'),
    path('v1/koan/get',views.get_koan, name='koan_get'),
    path('v1/koans/get',views.get_koans, name='koans_get'),
    path('v1/koans/titles',views.get_titles, name='koans_titles')

]